CloudForge: GitLab CI/CD for Seamless Website Delivery

Overview
Welcome to CloudForge – where Terraform magic meets AWS wonders! This project simplifies website deployment using GitLab CI/CD, Terraform, and AWS services like S3, CloudFront, and ACM.

Technologies Used
Terraform: Infrastructure provisioning made easy.
GitLab CI/CD: Automated deployment and continuous integration.
AWS Services:
S3 (Simple Storage Service): Storage for website content.
CloudFront: Content delivery network for accelerated performance.
ACM (AWS Certificate Manager): Manages SSL/TLS certificates for secure connections.

Project Setup

Organize Terraform Files:

Keep all Terraform files in the "Terraform" folder.

Configure GitLab Variables:

Navigate to project settings, select "CI/CD," and add AWS credentials and GitLab access token.

Initialize Terraform:

Run the terraform init command in the Terraform directory to configure the GitLab backend.

GitLab CI/CD Workflow:

Check .gitlab-ci.yml for a simple workflow that triggers on pushes to branches other than "main" and merge request events.

Step by step whole project: https://myproject.ltd/project12